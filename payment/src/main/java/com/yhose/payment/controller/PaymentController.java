package com.yhose.payment.controller;


import com.yhose.payment.dto.SaleRequestDto;
import com.yhose.payment.service.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController()
@RequestMapping("/pg")
public class PaymentController {
  @Autowired
  private PaymentService paymentService;

  @PostMapping("/do-payment")
  public void doPayment(@RequestBody SaleRequestDto req) {
    try {
      paymentService.doPayment(req);
    } catch (Exception e) {
      System.out.println("Error " + req.getBank());
    }
  }
}
