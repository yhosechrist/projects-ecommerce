package com.yhose.payment.dto.bca;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class OutboundRequest {
  private String orderId;
  private long amount;
}
