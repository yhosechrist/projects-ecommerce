package com.yhose.payment.dto;

import lombok.Data;

@Data
public class SaleRequestDto {
  private String bank;
  private String orderId;
  private long amount;
}
