package com.yhose.payment.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class NotificationDto {
  private String orderId;
  private long amount;
  private String status;
}
