package com.yhose.payment.dto.bca;

import lombok.Data;

@Data
public class OutboundResponse {
  private String orderId;
  private long amount;
  private String status;
  private long serverTime;
}
