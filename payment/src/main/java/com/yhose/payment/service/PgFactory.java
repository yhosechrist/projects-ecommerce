package com.yhose.payment.service;

import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class PgFactory {
  @Autowired
  @Qualifier("BCA")
  private PgService pgFactoryBca;


  @Autowired
  @Qualifier("BRI")
  private PgService pgFactoryBri;

  public PgService getPgService(String service) throws Exception {
    System.out.println("service " + service);
    switch (service) {
      case "BCA":
        return pgFactoryBca;
      case "BRI":
        return pgFactoryBri;
        default:
          throw new Exception();
    }
  }
}
