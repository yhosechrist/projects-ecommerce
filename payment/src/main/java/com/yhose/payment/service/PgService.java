package com.yhose.payment.service;

import com.yhose.payment.dto.SaleRequestDto;

public interface PgService {
  void sale(SaleRequestDto req);
}
