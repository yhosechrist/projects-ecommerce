package com.yhose.payment.service;

import com.yhose.payment.dto.NotificationDto;

public interface NotificationService {
  void send(NotificationDto req);
}
