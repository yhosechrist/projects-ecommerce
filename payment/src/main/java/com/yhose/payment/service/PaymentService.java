package com.yhose.payment.service;

import com.yhose.payment.dto.SaleRequestDto;

public interface PaymentService {
  void doPayment(SaleRequestDto req) throws Exception;
}
