package com.yhose.payment.service.impl;

import com.google.gson.Gson;
import com.yhose.payment.dto.NotificationDto;
import com.yhose.payment.service.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class ImplNotificationService implements NotificationService {
  @Autowired
  private KafkaTemplate<String, String> kafkaTemplate;

  @Value(value = "${kafka.topic.payment}")
  private String topic;

  @Override
  public void send(NotificationDto req) {
    this.kafkaTemplate.send(topic, new Gson().toJson(req));
  }
}
