package com.yhose.payment.service.impl;

import com.yhose.payment.dto.SaleRequestDto;
import com.yhose.payment.service.PaymentService;
import com.yhose.payment.service.PgFactory;
import com.yhose.payment.service.PgService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ImplPaymentService implements PaymentService {

  @Autowired
  private PgFactory pgFactory;

  @Override
  public void doPayment(SaleRequestDto req) throws Exception {
    PgService service = pgFactory.getPgService(req.getBank());
    System.out.println("Start Here");
    service.sale(req);
    System.out.println("End Here");
  }
}
