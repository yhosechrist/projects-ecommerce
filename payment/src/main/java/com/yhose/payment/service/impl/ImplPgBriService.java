package com.yhose.payment.service.impl;

import com.yhose.payment.dto.NotificationDto;
import com.yhose.payment.dto.SaleRequestDto;
import com.yhose.payment.dto.bca.OutboundRequest;
import com.yhose.payment.dto.bca.OutboundResponse;
import com.yhose.payment.service.NotificationService;
import com.yhose.payment.service.PgService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service("BRI")
public class ImplPgBriService implements PgService {

  @Autowired
  private NotificationService notificationService;

  @Value(value = "${sale.url.bri}")
  private String url;

  @Async
  @Override
  public void sale(SaleRequestDto req) {
    RestTemplate rt = new RestTemplate();
    HttpEntity<OutboundRequest> request = new HttpEntity<>(
        OutboundRequest.builder()
            .amount(req.getAmount())
            .orderId(req.getOrderId())
            .build()
    );
    ResponseEntity<OutboundResponse> response = rt.postForEntity(url, request, OutboundResponse.class);
    System.out.println("Request End : " + response.getBody().getStatus());
    notificationService.send(NotificationDto.builder()
        .amount(response.getBody().getAmount())
        .orderId(response.getBody().getOrderId())
        .status(response.getBody().getStatus())
        .build());
  }
}
