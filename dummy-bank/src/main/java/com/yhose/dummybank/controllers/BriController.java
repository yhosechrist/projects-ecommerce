package com.yhose.dummybank.controllers;

import com.yhose.dummybank.dto.bca.SaleRequestDto;
import com.yhose.dummybank.dto.bca.SaleResponseDto;
import java.util.Date;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController()
@RequestMapping("/bri")
public class BriController {

  @PostMapping("/sale")
  public SaleResponseDto sale(@RequestBody SaleRequestDto request)
      throws InterruptedException {
    Thread.sleep(10);
    var response = SaleResponseDto.builder()
        .amount(request.getAmount())
        .orderId(request.getOrderId())
        .status("SUCCESS")
        .serverTime(new Date().getTime())
        .build();
    return response;
  }
}
