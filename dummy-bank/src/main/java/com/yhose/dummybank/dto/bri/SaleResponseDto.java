package com.yhose.dummybank.dto.bri;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class SaleResponseDto {
  private String orderId;
  private long amount;
  private String status;
  private long serverTime;
}
