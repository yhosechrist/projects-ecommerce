package com.yhose.dummybank.dto.bri;

import lombok.Data;

@Data
public class SaleRequestDto {
  private String orderId;
  private long amount;
}
