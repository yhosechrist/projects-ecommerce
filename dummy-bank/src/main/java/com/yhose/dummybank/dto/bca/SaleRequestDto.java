package com.yhose.dummybank.dto.bca;

import lombok.Data;

@Data
public class SaleRequestDto {
  private String orderId;
  private long amount;
}
