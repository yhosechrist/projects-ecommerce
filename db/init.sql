-- payment.`transaction` definition

CREATE TABLE `transaction` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `order_id` varchar(100) DEFAULT NULL,
  `total_amount` bigint DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `created_at` DATETIME DEFAULT NULL,
  `updated_at` DATETIME DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `transaction_order_id_IDX` (`order_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- payment.transaction_detail definition

CREATE TABLE `transaction_detail` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `transaction_id` int unsigned DEFAULT NULL,
  `item` varchar(100) DEFAULT NULL,
  `qty` int DEFAULT NULL,
  `price` bigint DEFAULT NULL,
  `created_at` DATETIME DEFAULT NULL,
  `updated_at` DATETIME DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `transaction_detail_transaction_id_IDX` (`transaction_id`) USING BTREE,
  KEY `transaction_detail_item_IDX` (`item`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
