
# Ecommerce Simulation
Simulate checkout and payment process on Ecommerce




## Installation

Install docker

```bash
  cd projects-ecommerce
  docker compose up --build
```

Run projects
```bash
cd dummy-bank
mvn spring-boot:run
## You can access api on http://localhost:8082

cd payment
mvn spring-boot:run
## You can access api on http://localhost:8081

cd ecommerce
mvn spring-boot:run
## You can access api on http://localhost:8080

```

    
## Sequence Diagram
![Sequence Diagram](https://www.plantuml.com/plantuml/png/XP5DQiGm38NtFeKkq0jaKQRq0gM5Rel6KMTCR3crP8RSlev9Gdu8imb1Uf_q_7GHnQGvU9E54hoy0jaO0YLB3RpVoNOnAtpINoPHi_Hsmc_qhaKbS4f183Aa8r6G7eT0h3-tedtWq40Rvvf1h_0c0rluc2exzRZpYjmry4NSpXj0q9CPwt1Si4YbZonKjSUnwxViYKaCMHpVeCFV3azJo7tDO4P4KNCDevJnYaAcZ3wbK_bFBMX25hJg8g_HBJRUgW7PE7qulSyx-z8fVaqDjLvnFNTIdLvywg-HKzSLg2wVkKsaEV7YjsPnAPyS_3y0)






## API Reference

#### Get all items
Return all items available for the checkout process

```http
  GET /item/all
```


#### Checkout
Create transactions and make payments
```http
  POST /item/checkout
```

| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `bank`      | `String` | **Required**. bank used for payment process. Option Bank can be used (`BCA`, `BRI`) |
| `items.itemId`      | `String` | **Required**. itemId get from response API Get All Item |
| `items.qty`      | `Integer` | **Required**. item quantity |

```
curl --location 'localhost:8080/item/checkout' \
--header 'Content-Type: application/json' \
--header 'Accept: application/json' \
--data '{
    "items": [
        {
            "itemId": "654de411deef10b48c79f1ce",
            "qty": 2
        },
        {
            "itemId": "654de411deef10b48c79f207",
            "qty": 1
        }
    ],
    "bank": "BCA"
}'
```

### Check Status
Check the transaction status after the Checkout process
```http
  GET /transaction/check-status/{orderId}
```

| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `orderId`      | `String` | **Required**. orderId get from response API Checkout |


### Reporting
Goods sales report by date
```http
  GET /report/items?date={date}
```
| Parameter | Type     | Description                       |
| :-------- | :------- | :-------------------------------- |
| `date`      | `Date` | **Required**. date with format yyyy-mm-dd |

## Authors

- [@yhosec](https://github.com/yhosec)

