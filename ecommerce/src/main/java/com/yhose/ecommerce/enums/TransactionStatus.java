package com.yhose.ecommerce.enums;

public enum TransactionStatus {
  PENDING,
  DONE
}
