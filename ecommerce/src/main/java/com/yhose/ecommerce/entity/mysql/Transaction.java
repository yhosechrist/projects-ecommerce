package com.yhose.ecommerce.entity.mysql;

import com.yhose.ecommerce.enums.TransactionStatus;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "transaction")
@Data
@NoArgsConstructor
public class Transaction {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
  @GenericGenerator(name = "native", strategy = "native")
  private Integer id;
  private String orderId;
  private long totalAmount;
  private Date createdAt;
  private Date updatedAt;
  private TransactionStatus status;
}
