package com.yhose.ecommerce.entity.mongo;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Builder
@Document("items")
public class Item {

  @Id
  private String id;
  private String name;
  private long price;

}
