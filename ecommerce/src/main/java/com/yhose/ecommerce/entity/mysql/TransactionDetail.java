package com.yhose.ecommerce.entity.mysql;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "transaction_detail")
@Data
@Builder
@AllArgsConstructor
public class TransactionDetail {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
  @GenericGenerator(name = "native", strategy = "native")
  private Integer id;
  @ManyToOne
  @JoinColumn(name="transactionId")
  private Transaction transactionId;
  private String item;
  private int qty;
  private long price;
  private Date createdAt;
  private Date updatedAt;
}
