package com.yhose.ecommerce.repository.mysql;

import com.yhose.ecommerce.dto.endpoint.report.ItemsResponseDto;
import com.yhose.ecommerce.entity.mysql.Transaction;
import com.yhose.ecommerce.entity.mysql.TransactionDetail;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface TransactionDetailRepository extends JpaRepository<TransactionDetail, Integer> {

  @Query(value = "SELECT " +
      "new com.yhose.ecommerce.dto.endpoint.report.ItemsResponseDto(item, sum(qty), sum(price*qty)) " +
      "FROM TransactionDetail " +
      "WHERE createdAt BETWEEN ?1 AND ?2 " +
      "GROUP BY item")
  List<ItemsResponseDto> findItemsReport(Date start, Date end);
}
