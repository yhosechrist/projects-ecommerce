package com.yhose.ecommerce.repository.mysql;

import com.yhose.ecommerce.entity.mysql.Transaction;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface TransactionRepository extends JpaRepository<Transaction, Integer> {
  @Query(value = "SELECT t FROM Transaction t where t.orderId = ?1")
  Optional<Transaction> findByOrderId(String orderId);
}
