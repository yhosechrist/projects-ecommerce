package com.yhose.ecommerce.repository.mongo.impl;

import com.yhose.ecommerce.entity.mongo.Item;
import com.yhose.ecommerce.repository.mongo.CustomItemRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

@Repository
public class ImplCustomItemRepository implements CustomItemRepository {
  @Autowired
  private MongoTemplate mongoTemplate;

  @Override
  public List<Item> findByIds(List<String> ids) {
    Query query = new Query();
    query.addCriteria(Criteria.where("_id").in(ids));
    return mongoTemplate.find(query, Item.class);
  }
}
