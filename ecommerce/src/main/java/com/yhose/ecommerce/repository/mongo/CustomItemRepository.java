package com.yhose.ecommerce.repository.mongo;

import com.yhose.ecommerce.entity.mongo.Item;
import java.util.List;

public interface CustomItemRepository {
  List<Item> findByIds(List<String> ids);
}
