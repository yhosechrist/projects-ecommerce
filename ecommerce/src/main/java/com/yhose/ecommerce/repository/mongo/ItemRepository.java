package com.yhose.ecommerce.repository.mongo;

import com.yhose.ecommerce.entity.mongo.Item;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ItemRepository extends MongoRepository<Item, String> {
}
