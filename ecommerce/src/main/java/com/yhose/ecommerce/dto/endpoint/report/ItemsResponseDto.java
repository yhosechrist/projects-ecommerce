package com.yhose.ecommerce.dto.endpoint.report;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ItemsResponseDto {
  private String name;
  private Long totalQty;
  private Long totalAmount;
}
