package com.yhose.ecommerce.dto.endpoint.items;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ItemResponseDto {
  private String itemId;
  private String name;
  private long price;
}
