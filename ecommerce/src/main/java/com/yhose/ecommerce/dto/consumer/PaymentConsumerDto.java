package com.yhose.ecommerce.dto.consumer;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class PaymentConsumerDto {
  private String orderId;
  private long amount;
  private String status;
}
