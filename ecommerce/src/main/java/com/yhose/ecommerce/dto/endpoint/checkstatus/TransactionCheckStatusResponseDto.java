package com.yhose.ecommerce.dto.endpoint.checkstatus;

import com.yhose.ecommerce.enums.TransactionStatus;
import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class TransactionCheckStatusResponseDto implements Serializable {
  private String orderId;
  private TransactionStatus status;
  private long amount;
}
