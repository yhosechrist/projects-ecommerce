package com.yhose.ecommerce.dto.outbound;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class OutboundPaymentResponseDto {
  private String orderId;
  private long amount;
  private String status;
  private long serverTime;
}

