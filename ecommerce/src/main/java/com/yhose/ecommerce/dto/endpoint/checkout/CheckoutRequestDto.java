package com.yhose.ecommerce.dto.endpoint.checkout;

import com.yhose.ecommerce.enums.Bank;
import java.io.Serializable;
import java.util.List;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CheckoutRequestDto implements Serializable {
  private List<ItemDto> items;
  private Bank bank;
}
