package com.yhose.ecommerce.dto.endpoint.checkout;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ItemDto {
  private String itemId;
  private int qty;
}
