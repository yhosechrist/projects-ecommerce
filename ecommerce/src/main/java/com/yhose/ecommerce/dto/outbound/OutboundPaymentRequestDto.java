package com.yhose.ecommerce.dto.outbound;

import com.yhose.ecommerce.enums.Bank;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class OutboundPaymentRequestDto {
  private Bank bank;
  private String orderId;
  private long amount;
}

