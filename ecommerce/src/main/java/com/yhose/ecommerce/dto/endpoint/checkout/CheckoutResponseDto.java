package com.yhose.ecommerce.dto.endpoint.checkout;

import com.yhose.ecommerce.enums.TransactionStatus;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CheckoutResponseDto {
  private String orderId;
  private TransactionStatus status;
}
