package com.yhose.ecommerce.consumer;


import com.google.gson.Gson;
import com.yhose.ecommerce.dto.consumer.PaymentConsumerDto;
import com.yhose.ecommerce.enums.TransactionStatus;
import com.yhose.ecommerce.repository.mysql.TransactionRepository;
import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Caching;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
public class PaymentConsumer {

  @Autowired
  private TransactionRepository transactionRepository;



  @KafkaListener(topics="${kafka.topic.payment}", groupId="${kafka.group.payment}")
  public void consume(String message) {
    PaymentConsumerDto data = new Gson().fromJson(message, PaymentConsumerDto.class);
    updateStatus(data);
  }

  @Caching(evict = {
      @CacheEvict(value = "payment-status", key = "#data.orderId")
  })
  private void updateStatus(PaymentConsumerDto data) {
    transactionRepository.findByOrderId(data.getOrderId()).ifPresent(
        transaction -> {
          transaction.setStatus(TransactionStatus.DONE);
          transaction.setUpdatedAt(new Date());
          transactionRepository.save(transaction);
        }
    );
  }

}
