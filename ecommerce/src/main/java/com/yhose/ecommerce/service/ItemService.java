package com.yhose.ecommerce.service;

import com.yhose.ecommerce.dto.endpoint.checkout.CheckoutRequestDto;
import com.yhose.ecommerce.dto.endpoint.checkout.CheckoutResponseDto;
import com.yhose.ecommerce.dto.endpoint.items.ItemResponseDto;
import com.yhose.ecommerce.dto.endpoint.report.ItemsResponseDto;
import java.util.Date;
import java.util.List;

public interface ItemService {
  CheckoutResponseDto doCheckout(CheckoutRequestDto request);
  List<ItemResponseDto> getAllData();

  List<ItemsResponseDto> getReportData(Date date);
}
