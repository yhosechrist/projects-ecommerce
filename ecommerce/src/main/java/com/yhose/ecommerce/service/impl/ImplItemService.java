package com.yhose.ecommerce.service.impl;

import com.yhose.ecommerce.dto.endpoint.checkout.CheckoutResponseDto;
import com.yhose.ecommerce.dto.endpoint.report.ItemsResponseDto;
import com.yhose.ecommerce.entity.mongo.Item;
import com.yhose.ecommerce.dto.endpoint.checkout.CheckoutRequestDto;
import com.yhose.ecommerce.dto.endpoint.checkout.ItemDto;
import com.yhose.ecommerce.dto.endpoint.items.ItemResponseDto;
import com.yhose.ecommerce.dto.outbound.OutboundPaymentRequestDto;
import com.yhose.ecommerce.entity.mysql.Transaction;
import com.yhose.ecommerce.entity.mysql.TransactionDetail;
import com.yhose.ecommerce.enums.Bank;
import com.yhose.ecommerce.enums.ResponseCode;
import com.yhose.ecommerce.enums.TransactionStatus;
import com.yhose.ecommerce.exception.BusinessLogicException;
import com.yhose.ecommerce.repository.mongo.CustomItemRepository;
import com.yhose.ecommerce.repository.mongo.ItemRepository;
import com.yhose.ecommerce.repository.mysql.TransactionDetailRepository;
import com.yhose.ecommerce.repository.mysql.TransactionRepository;
import com.yhose.ecommerce.service.ItemService;
import com.yhose.ecommerce.service.PaymentService;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ImplItemService implements ItemService {
  @Autowired
  private ItemRepository itemRepository;

  @Autowired
  private CustomItemRepository customItemRepository;

  @Autowired
  private TransactionRepository transactionRepository;

  @Autowired
  private TransactionDetailRepository transactionDetailRepository;
  @Autowired
  private PaymentService paymentService;

  @Override
  public CheckoutResponseDto doCheckout(CheckoutRequestDto request) throws BusinessLogicException {
    List<Item> items = customItemRepository.findByIds(request.getItems().stream().map(
        item -> item.getItemId()
    ).collect(Collectors.toList()));
    if (items.size() != request.getItems().size()) {
      throw new BusinessLogicException(ResponseCode.BAD_REQUEST.name(), "Invalid items request");
    }
    long totalAmount = calculateTotalAmount(items, request.getItems());
    String orderId = UUID.randomUUID().toString();

    doPayment(request.getBank(), totalAmount, orderId);
    storeTransactionToDatabase(totalAmount, orderId, items, request.getItems());

    return CheckoutResponseDto.builder()
        .orderId(orderId)
        .status(TransactionStatus.PENDING)
        .build();
  }

  private Transaction storeTransactionToDatabase(long amount, String orderId, List<Item> items, List<ItemDto> itemsRequest) {
    Date now = new Date();
    Map<String, Integer> qty = itemsRequest.stream().collect(Collectors.toMap(ItemDto::getItemId, ItemDto::getQty));
    Transaction transaction = new Transaction();
    transaction.setOrderId(orderId);
    transaction.setStatus(TransactionStatus.PENDING);
    transaction.setTotalAmount(amount);
    transaction.setCreatedAt(now);
    transaction.setUpdatedAt(now);
    transaction = transactionRepository.save(transaction);
    transactionDetailRepository.saveAll(
        items.stream().map(item -> TransactionDetail.builder()
            .item(item.getName())
            .price(item.getPrice())
            .qty(qty.get(item.getId()))
            .createdAt(now)
            .updatedAt(now)
            .build()
        ).collect(Collectors.toList())
    );
    return transaction;
  }

  private long calculateTotalAmount(List<Item> items, List<ItemDto> itemsRequest) {
    Map<String, Integer> qty = itemsRequest.stream().collect(Collectors.toMap(ItemDto::getItemId, ItemDto::getQty));
    return items.stream().map(
        item -> item.getPrice() * qty.get(item.getId())
    ).collect(Collectors.toList()).stream().reduce(0L, Long::sum);
  }

  private void doPayment(Bank bank, long totalAmount, String orderId) {
    OutboundPaymentRequestDto paymentRequest = OutboundPaymentRequestDto.builder()
        .bank(bank)
        .orderId(orderId)
        .amount(totalAmount)
        .build();
    paymentService.doPayment(paymentRequest);
  }

  @Override
  public List<ItemResponseDto> getAllData() {
    List<Item> items = itemRepository.findAll();
    return items.stream().map(
        item -> ItemResponseDto.builder()
            .itemId(item.getId())
            .name(item.getName())
            .price(item.getPrice())
            .build()
    ).collect(Collectors.toList());
  }

  @Override
  public List<ItemsResponseDto> getReportData(Date date) {
    return transactionDetailRepository.findItemsReport(
        atStartOfDay(date),
        atEndOfDay(date)
    );
  }

  public Date atStartOfDay(Date date) {
    LocalDateTime localDateTime = dateToLocalDateTime(date);
    LocalDateTime startOfDay = localDateTime.with(LocalTime.MIN);
    return localDateTimeToDate(startOfDay);
  }

  public Date atEndOfDay(Date date) {
    LocalDateTime localDateTime = dateToLocalDateTime(date);
    LocalDateTime endOfDay = localDateTime.with(LocalTime.MAX);
    return localDateTimeToDate(endOfDay);
  }

  private LocalDateTime dateToLocalDateTime(Date date) {
    return LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault());
  }
  private Date localDateTimeToDate(LocalDateTime localDateTime) {
    return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
  }
}
