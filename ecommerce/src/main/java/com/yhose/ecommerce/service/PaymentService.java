package com.yhose.ecommerce.service;

import com.yhose.ecommerce.dto.outbound.OutboundPaymentRequestDto;
import com.yhose.ecommerce.dto.endpoint.checkstatus.TransactionCheckStatusResponseDto;

public interface PaymentService {
  void doPayment(OutboundPaymentRequestDto request);
  TransactionCheckStatusResponseDto checkStatus(String orderId);
}
