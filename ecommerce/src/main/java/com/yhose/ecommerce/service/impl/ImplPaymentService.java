package com.yhose.ecommerce.service.impl;

import com.yhose.ecommerce.dto.outbound.OutboundPaymentRequestDto;
import com.yhose.ecommerce.dto.outbound.OutboundPaymentResponseDto;
import com.yhose.ecommerce.dto.endpoint.checkstatus.TransactionCheckStatusResponseDto;
import com.yhose.ecommerce.repository.mysql.TransactionRepository;
import com.yhose.ecommerce.service.PaymentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Slf4j
@Service
public class ImplPaymentService implements PaymentService {
  @Autowired
  private TransactionRepository transactionRepository;

  @Async
  public void doPayment(OutboundPaymentRequestDto req) {
    RestTemplate rt = new RestTemplate();
    String url = "http://localhost:8081/pg/do-payment";
    HttpEntity<OutboundPaymentRequestDto> request = new HttpEntity<>(
        OutboundPaymentRequestDto.builder()
            .bank(req.getBank())
            .amount(req.getAmount())
            .orderId(req.getOrderId())
            .build()
    );
    ResponseEntity<OutboundPaymentResponseDto> response = rt.postForEntity(url, request, OutboundPaymentResponseDto.class);
    log.info("Response Payment ", response);
  }

  @Override
  @Cacheable(value = "payment-status", key = "#orderId")
  public TransactionCheckStatusResponseDto checkStatus(String orderId) {
    log.info("Check status order id ", orderId);
    return transactionRepository.findByOrderId(orderId)
        .map(transaction -> TransactionCheckStatusResponseDto.builder()
                .orderId(transaction.getOrderId())
                .status(transaction.getStatus())
                .amount(transaction.getTotalAmount())
                .build()
        ).orElse(null);
  }
}
