package com.yhose.ecommerce.controller;

import com.yhose.ecommerce.dto.endpoint.checkstatus.TransactionCheckStatusResponseDto;
import com.yhose.ecommerce.service.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController()
@RequestMapping("/transaction")
public class TransactionController {
  @Autowired
  private PaymentService paymentService;

  @GetMapping("/check-status/{orderId}")
  public TransactionCheckStatusResponseDto checkStatus(@PathVariable String orderId) {
    return paymentService.checkStatus(orderId);
  }
}
