package com.yhose.ecommerce.controller;

import com.yhose.ecommerce.dto.endpoint.report.ItemsResponseDto;
import com.yhose.ecommerce.service.ItemService;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController()
@RequestMapping("/report")
public class ReportController {
  @Autowired
  private ItemService itemService;

  @GetMapping("/items")
  public List<ItemsResponseDto> checkStatus(@RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date date) {
    return itemService.getReportData(date);
  }
}
