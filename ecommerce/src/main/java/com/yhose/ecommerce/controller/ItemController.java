package com.yhose.ecommerce.controller;

import com.yhose.ecommerce.dto.BaseResponseDto;
import com.yhose.ecommerce.dto.endpoint.checkout.CheckoutRequestDto;
import com.yhose.ecommerce.dto.endpoint.checkout.CheckoutResponseDto;
import com.yhose.ecommerce.dto.endpoint.items.ItemResponseDto;
import com.yhose.ecommerce.enums.ResponseCode;
import com.yhose.ecommerce.exception.BusinessLogicException;
import com.yhose.ecommerce.service.ItemService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController()
@RequestMapping("/item")
public class ItemController {
  @Autowired
  private ItemService itemService;

  @GetMapping("/all")
  public List<ItemResponseDto> all() {
    return itemService.getAllData();
  }

  @PostMapping("/checkout")
  public BaseResponseDto<CheckoutResponseDto> doPayment(@RequestBody CheckoutRequestDto req) {
    try {
      return BaseResponseDto.<CheckoutResponseDto>builder()
          .data(itemService.doCheckout(req))
          .code(ResponseCode.SUCCESS.name())
          .message(ResponseCode.SUCCESS.name())
          .build();
    } catch (BusinessLogicException e) {
      return BaseResponseDto.<CheckoutResponseDto>builder()
          .code(e.getCode())
          .message(e.getMessage())
          .build();
    }
  }
}
