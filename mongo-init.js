db.createCollection('items');
db.items.insertOne(
  {
    name: 'Beras Ponni Bharathi 25 kg',
    price: 300
  }
);
db.items.insertOne(
  {
    name: 'Beras Cap Rajekwesi 25 kg',
    price: 3100
  }
);
db.items.insertOne(
  {
    name: 'Beras Medium Pulen Cap 4 Mata Merah 25 kg',
    price: 1300
  }
);
db.items.insertOne(
  {
    name: 'Beras Cap Jambal 25',
    price: 9600
  }
);
db.items.insertOne(
  {
    name: 'Beras Sadon 25 kg',
    price: 1400
  }
);
db.items.insertOne(
  {
    name: 'Beras Cap Strawberry 25 kg',
    price: 5800
  }
);
db.items.insertOne(
  {
    name: 'Beras Cap Mawar 25 kg',
    price: 9100
  }
);
db.items.insertOne(
  {
    name: 'Beras Melon Super 25 kg',
    price: 8100
  }
);
db.items.insertOne(
  {
    name: 'Beras Punel Cap Tiga Layar 25 kg',
    price: 7600
  }
);
db.items.insertOne(
  {
    name: 'Beras Ramos Kepala Bunga 25 kg',
    price: 8200
  }
);
db.items.insertOne(
  {
    name: 'Beras Kepala Slyp Super Cap Kembang 25 kg',
    price: 7400
  }
);
db.items.insertOne(
  {
    name: 'Beras Setra Ramos Cap Rezeki Gambar Kembang 25 kg',
    price: 8100
  }
);
db.items.insertOne(
  {
    name: 'Beras Cap Tiga Jambu 25 kg',
    price: 300
  }
);
db.items.insertOne(
  {
    name: 'Beras Mentari 25 kg',
    price: 1800
  }
);
db.items.insertOne(
  {
    name: 'Beras Cap Ayam Mahkota 25 kg',
    price: 7900
  }
);
db.items.insertOne(
  {
    name: 'Beras Rojolele Delanggu Solo 25 kg',
    price: 8100
  }
);
db.items.insertOne(
  {
    name: 'Beras Kepala Slyp Setra Ramos BMW Cianjur 25 kg',
    price: 3400
  }
);
db.items.insertOne(
  {
    name: 'Beras Setra Ramos Saigon 25 kg',
    price: 4300
  }
);
db.items.insertOne(
  {
    name: 'Beras Cap Topi Koki Slyp Kuning 25 kg',
    price: 5100
  }
);
db.items.insertOne(
  {
    name: 'Beras Cap Rumah Limas Setra Ramos 25 kg',
    price: 3600
  }
);
db.items.insertOne(
  {
    name: 'Beras Cap Petruk 25 kg',
    price: 9800
  }
);
db.items.insertOne(
  {
    name: 'Beras Mampan RMK 25 kg',
    price: 8200
  }
);
db.items.insertOne(
  {
    name: 'Beras Bengawan Super 25 kg',
    price: 7700
  }
);
db.items.insertOne(
  {
    name: 'Beras Ngawiti 25 kg',
    price: 3800
  }
);
db.items.insertOne(
  {
    name: 'Beras Setra Ramos Cap Kepala Pundi 25 kg',
    price: 9700
  }
);
db.items.insertOne(
  {
    name: 'Beras Pandan Wangi 25 kg',
    price: 1900
  }
);
db.items.insertOne(
  {
    name: 'Beras Jembar AAA 25 kg',
    price: 5200
  }
);
db.items.insertOne(
  {
    name: 'Beras Padang Solok Anak Daro 25 kg',
    price: 9800
  }
);
db.items.insertOne(
  {
    name: 'Beras Setra Ramos Kepala Bunga 25 kg',
    price: 5500
  }
);
db.items.insertOne(
  {
    name: 'Beras Cap Kepala DM 25 kg',
    price: 300
  }
);
db.items.insertOne(
  {
    name: 'Beras BMW 25 kg Rp',
    price: 4700
  }
);
db.items.insertOne(
  {
    name: 'Beras Idola 25 kg Rp',
    price: 3200
  }
);
db.items.insertOne(
  {
    name: 'Clear Sachet Besar',
    price: 2100
  }
);
db.items.insertOne(
  {
    name: 'Head & Shoulder',
    price: 5500
  }
);
db.items.insertOne(
  {
    name: 'Pantene Conditioner',
    price: 5000
  }
);
db.items.insertOne(
  {
    name: 'Pantene Shampo Sachet',
    price: 8500
  }
);
db.items.insertOne(
  {
    name: 'Rejoice Rich Sachet',
    price: 1600
  }
);
db.items.insertOne(
  {
    name: 'Shampo Dove Sachet',
    price: 8400
  }
);
db.items.insertOne(
  {
    name: 'Shampo Lifebuoy Botol',
    price: 8300
  }
);
db.items.insertOne(
  {
    name: 'Shampo Lifebuoy Sachet',
    price: 7300
  }
);
db.items.insertOne(
  {
    name: 'Shampo Sunsilk Sachet',
    price: 2300
  }
);
db.items.insertOne(
  {
    name: 'Harpic 200 ml',
    price: 3200
  }
);
db.items.insertOne(
  {
    name: 'So Klin Lantai Refill 800 ml',
    price: 3200
  }
);
db.items.insertOne(
  {
    name: 'Super Pell 400 ml',
    price: 1200
  }
);
db.items.insertOne(
  {
    name: 'Super Pell 800 ml',
    price: 2700
  }
);
db.items.insertOne(
  {
    name: 'Vixal Botol 500 ml',
    price: 5900
  }
);
db.items.insertOne(
  {
    name: 'Vixal Botol 800 ml',
    price: 6700
  }
);
db.items.insertOne(
  {
    name: 'Wipol Ref 450 ml',
    price: 9700
  }
);
db.items.insertOne(
  {
    name: 'Wipol Ref 800 ml',
    price: 2600
  }
);
db.items.insertOne(
  {
    name: 'Downy 25 ml/1000',
    price: 2800
  }
);
db.items.insertOne(
  {
    name: 'Kispray Biasa Sach',
    price: 2000
  }
);
db.items.insertOne(
  {
    name: 'Molto Ultra 1 X Bilas 15 ml',
    price: 8400
  }
);
db.items.insertOne(
  {
    name: 'Molto Ultra 1 X Bilas 25 ml',
    price: 2200
  }
);
db.items.insertOne(
  {
    name: 'Vanish Botol 180 ml',
    price: 6900
  }
);
db.items.insertOne(
  {
    name: 'Vanish Refil 150 ml',
    price: 1600
  }
);
db.items.insertOne(
  {
    name: 'Vanish Renteng Bubuk',
    price: 9200
  }
);
db.items.insertOne(
  {
    name: 'Vanish Renteng Cair',
    price: 4600
  }
);
db.items.insertOne(
  {
    name: 'Good Day Biasa',
    price: 6000
  }
);
db.items.insertOne(
  {
    name: 'Good Day Capucino Biasa',
    price: 1300
  }
);
db.items.insertOne(
  {
    name: 'Good Day Mocafrio',
    price: 5000
  }
);
db.items.insertOne(
  {
    name: 'Indocafé Capucino',
    price: 1800
  }
);
db.items.insertOne(
  {
    name: 'Indocafé Coffeemix',
    price: 900
  }
);
db.items.insertOne(
  {
    name: 'Kapal Api Spesial 30 gr',
    price: 7800
  }
);
db.items.insertOne(
  {
    name: 'Kapal Api Spesial 65 gr',
    price: 5200
  }
);
db.items.insertOne(
  {
    name: 'Kapal Api Spesial 165 gr',
    price: 1600
  }
);
db.items.insertOne(
  {
    name: 'Kapal Api Spesial 380 gr Silver',
    price: 5400
  }
);
db.items.insertOne(
  {
    name: 'Kopi ABC Mocca',
    price: 4200
  }
);
db.items.insertOne(
  {
    name: 'Kopi ABC Susu',
    price: 400
  }
);
db.items.insertOne(
  {
    name: 'Kopi ABC WC Instan',
    price: 600
  }
);
db.items.insertOne(
  {
    name: 'Kopi Luwak 3 Rasa',
    price: 2900
  }
);
db.items.insertOne(
  {
    name: 'Kopi Luwak White Coffie',
    price: 9000
  }
);
db.items.insertOne(
  {
    name: 'Kopi Top Toraja 165 gr',
    price: 3400
  }
);
db.items.insertOne(
  {
    name: 'Nescafe Classic',
    price: 4600
  }
);
db.items.insertOne(
  {
    name: 'Torabika Cafe',
    price: 1300
  }
);
db.items.insertOne(
  {
    name: 'Torabika Capucino',
    price: 8000
  }
);
db.items.insertOne(
  {
    name: 'Torabika Creamylatte',
    price: 5700
  }
);
db.items.insertOne(
  {
    name: 'Torabika Moka',
    price: 5500
  }
);
db.items.insertOne(
  {
    name: 'Torabika Susu',
    price: 1400
  }
);
db.items.insertOne(
  {
    name: 'Adem Sari Sachet',
    price: 2800
  }
);
db.items.insertOne(
  {
    name: 'Anget Sari Susu Jahe',
    price: 4600
  }
);
db.items.insertOne(
  {
    name: 'Jahe Susu Sidomuncul',
    price: 7100
  }
);
db.items.insertOne(
  {
    name: 'Jahe Wangi Intra',
    price: 3400
  }
);
db.items.insertOne(
  {
    name: 'Lemon Tea Maxtea',
    price: 4600
  }
);
db.items.insertOne(
  {
    name: 'Madurasa Dus',
    price: 1300
  }
);
db.items.insertOne(
  {
    name: 'Marimas',
    price: 8000
  }
);
db.items.insertOne(
  {
    name: 'Nutrisari Biasa',
    price: 3400
  }
);
db.items.insertOne(
  {
    name: 'Nutrisari Wedank Bajigur',
    price: 4600
  }
);
db.items.insertOne(
  {
    name: 'Pop Ice',
    price: 1300
  }
);
db.items.insertOne(
  {
    name: 'Sekoteng ABC',
    price: 8000
  }
);
db.items.insertOne(
  {
    name: 'Segar Dingin',
    price: 3200
  }
);
db.items.insertOne(
  {
    name: 'Teh Sisri',
    price: 1200
  }
);
db.items.insertOne(
  {
    name: 'Teh Tarikk Maxtea',
    price: 2700
  }
);
db.items.insertOne(
  {
    name: 'Top Ice',
    price: 5900
  }
);
db.items.insertOne(
  {
    name: 'Vegeta Biasa',
    price: 6700
  }
);
db.items.insertOne(
  {
    name: 'Vegeta Herbal',
    price: 9700
  }
);
db.items.insertOne(
  {
    name: 'Fanta 1 liter',
    price: 2600
  }
);
db.items.insertOne(
  {
    name: 'Sprite 1 liter',
    price: 2800
  }
);
db.items.insertOne(
  {
    name: 'Coca-Cola 1 liter',
    price: 2000
  }
);